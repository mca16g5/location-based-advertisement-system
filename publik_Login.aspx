﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Publik.master" AutoEventWireup="true" CodeFile="publik_Login.aspx.cs" Inherits="publik_Login" %>

<%--<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
    .style2
    {
        height: 26px;
    }
</style>
</asp:Content>--%>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <table class="style1">
        <tr>
            <td style="width: 90px">
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp</td>
            <td style="width: 530px">
                <asp:TextBox ID="txt_username" runat="server" placeholder="Username" 
                    BorderStyle="None" BackColor="#666699" BorderColor="#666699" Width="184px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                    ControlToValidate="txt_username" ErrorMessage="***" ForeColor="#CC0000"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="style2" style="width: 90px">
                
            </td>
            <td class="style2" style="width: 530px">
                <asp:TextBox ID="txt_password" runat="server" TextMode="Password" 
                    placeholder="Password" BorderStyle="None" BackColor="#666699" 
                    BorderColor="#666699" Width="184px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                    ControlToValidate="txt_password" ErrorMessage="***" ForeColor="#CC0000"></asp:RequiredFieldValidator>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" 
                    ControlToValidate="txt_password" ErrorMessage="incorrect password"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td style="width: 90px; height: 30px;">
                </td>
            <td style="width: 530px; height: 30px;">
                <asp:Button ID="btn_login" runat="server" Text="Login" 
                    onclick="btn_login_Click" Height="26px" Width="184px" BackColor="#666699" 
                    BorderColor="#666699" BorderStyle="None" />
            </td>
        </tr>
        <tr></tr>
        <tr></tr>
        <tr>
            <td style="width: 90px">
                &nbsp;</td>
            <td style="width: 530px">
                &nbsp;</td>
        </tr>
    </table>
</asp:Content>

