﻿<%@ Page Title="" Language="C#" MasterPageFile="~/shop.master" AutoEventWireup="true" CodeFile="shop_view_offer.aspx.cs" Inherits="shop_view_offer" %>

<%--<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>--%>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:MultiView ID="MultiView1" runat="server">
        <asp:View ID="View1" runat="server">
            <asp:DataGrid ID="DataGrid1" runat="server" AutoGenerateColumns="False" 
                onitemcommand="DataGrid1_ItemCommand" 
               BackColor="White" BorderColor="White" BorderStyle="Ridge" BorderWidth="2px" 
                            GridLines="None" style="margin-top: 0px" Width="651px">
                <Columns>
                    <asp:BoundColumn DataField="offerid" HeaderText="OFFER ID"></asp:BoundColumn>
                    <asp:BoundColumn DataField="offers" HeaderText="OFFER"></asp:BoundColumn>
                    <asp:BoundColumn DataField="validfrom" HeaderText="VALID FROM">
                    </asp:BoundColumn>
                    <asp:BoundColumn DataField="validto" HeaderText="VALID TO"></asp:BoundColumn>
                    <asp:ButtonColumn CommandName="EDIT" Text="EDIT"></asp:ButtonColumn>
                    <asp:ButtonColumn CommandName="DELETE" Text="DELETE"></asp:ButtonColumn>
                </Columns>
                <FooterStyle BackColor="#C6C3C6" ForeColor="Black" />
                <HeaderStyle BackColor="#4A3C8C" Font-Bold="True" ForeColor="#E7E7FF" />
                <ItemStyle BackColor="#DEDFDE" ForeColor="Black" />
                <PagerStyle BackColor="#C6C3C6" ForeColor="Black" HorizontalAlign="Right" />
                <SelectedItemStyle BackColor="#9471DE" Font-Bold="True" ForeColor="White" />
            </asp:DataGrid>
        </asp:View>
        <asp:View ID="View2" runat="server">
            <table class="style1">
                <tr>
                    <td style="height: 17px">
                        </td>
                    <td style="height: 17px">
                        </td>
                </tr>
                <tr>
                    <td style="color: #000000; background-color: #FFFFFF">
                        OFFER</td>
                    <td>
                        <asp:TextBox ID="TextBox2" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td style="color: #000000; background-color: #FFFFFF">
                        VALID FROM</td>
                    <td>
                        <asp:DropDownList ID="DropDownList1" runat="server">
                        </asp:DropDownList>
                        <asp:DropDownList ID="DropDownList2" runat="server">
                        </asp:DropDownList>
                        <asp:DropDownList ID="DropDownList3" runat="server">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td style="color: #000000; background-color: #FFFFFF">
                        VALID TO</td>
                    <td>
                        <asp:DropDownList ID="DropDownList4" runat="server">
                        </asp:DropDownList>
                        <asp:DropDownList ID="DropDownList5" runat="server">
                        </asp:DropDownList>
                        <asp:DropDownList ID="DropDownList6" runat="server">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;</td>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr>
                    <td>
                        &nbsp;</td>
                    <td>
                        <asp:Button ID="btn_update" runat="server" onclick="btn_update_Click" 
                            Text="UPDATE" />
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;</td>
                    <td>
                        &nbsp;</td>
                </tr>
            </table>
        </asp:View>
    </asp:MultiView>
    <br />
</asp:Content>

