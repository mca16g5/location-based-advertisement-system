﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class shop_status_updation : System.Web.UI.Page
{
    dbop db = new dbop();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            for (int i = 1; i < 32; i++)

            {
                if (i < 10)
                {
                    DropDownList1.Items.Add("0" + i.ToString());
                }
                else
                {
                    DropDownList1.Items.Add(i.ToString());
                }
            }
            for (int i = 1; i < 13; i++)
            {
                if (i < 10)
                {
                    DropDownList2.Items.Add("0" + i.ToString());
                }
                else
                {
                    DropDownList2.Items.Add(i.ToString());
                }
            }
            for (int i =2000; i <2030; i++)
            {
                DropDownList3.Items.Add(i.ToString());
            }
            DropDownList1.Items.Insert(0, "Day");
            DropDownList2.Items.Insert(0, "Month");
            DropDownList3.Items.Insert(0, "Year");
            string q = "select * from status where shopid='" + Session["sid"] + "'";
            DataTable dt = db.ret(q);
            DataGrid1.DataSource = dt;
            DataGrid1.DataBind();
            MultiView1.SetActiveView(View1);
        }

    }
    
    protected void btn_update_Click(object sender, EventArgs e)
    {
        string  date=(DropDownList2.Text+"/"+DropDownList1.Text+"/"+DropDownList3.Text);
        if (btn_update.Text == "Update")
        {
           string s= "update status set status='" + txt_status.Text + "' where shopid='" + Session["sid"] + "' and lastdate='" + date + "'";
            db.nonret(s);
        }
        else
        {

            string s = "insert into status values(" + Session["sid"].ToString() + ",'" + txt_status.Text + "','" + date + "')";
            db.nonret(s);
        }
        string q = "select * from status where shopid='" + Session["sid"] + "'";
        DataTable dt = db.ret(q);
        DataGrid1.DataSource = dt;
        DataGrid1.DataBind();
        MultiView1.SetActiveView(View1);
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        MultiView1.SetActiveView(View2);
        txt_status.Text = "";

        DropDownList1.SelectedIndex = 0;
        DropDownList2.SelectedIndex = 0;
        DropDownList3.SelectedIndex = 0;
    }
    protected void DataGrid1_ItemCommand(object source, DataGridCommandEventArgs e)
    {
        string q;
        if (e.CommandName == "edit")
        {

            String[] ar1 = { "-", "/" };

            txt_status.Text = e.Item.Cells[1].Text;
            string[] ar = e.Item.Cells[0].Text.Split(ar1,StringSplitOptions.RemoveEmptyEntries);
            DropDownList1.SelectedValue = ar[1];
            DropDownList2.SelectedValue = ar[0];
            DropDownList3.SelectedValue = ar[2];
            MultiView1.SetActiveView(View2);
            btn_update.Text = "Update";

        }
        else
        {
            q = "delete from status where shopid='" + Session["sid"].ToString() + "' and lastdate='" + e.Item.Cells[0].Text + "'";
            db.nonret(q);
             q = "select * from status where shopid='" + Session["sid"] + "'";
            DataTable dt = db.ret(q);
            DataGrid1.DataSource = dt;
            DataGrid1.DataBind();
           

        }
    }
}