﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin.master" AutoEventWireup="true" CodeFile="admin_Shop_approval.aspx.cs" Inherits="admin_Shop_approval" %>

<%--<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        .style2
        {
            height: 26px;
        }
    </style>
</asp:Content>--%>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:MultiView ID="MultiView1" runat="server" 
        onactiveviewchanged="MultiView1_ActiveViewChanged">
        <br />
        <asp:View ID="View1" runat="server">
            <asp:DataGrid ID="DataGrid1" runat="server" AutoGenerateColumns="False" 
                onitemcommand="DataGrid1_ItemCommand" BackColor="#CC0000" 
                BorderColor="White" BorderStyle="Ridge" BorderWidth="2px" 
                Font-Bold="False" Font-Italic="True" Font-Overline="False" 
                Font-Size="Medium" Font-Strikeout="False" Font-Underline="False" 
                ForeColor="#990000" GridLines="None" Height="213px" Width="718px">
                <Columns>
                    <asp:BoundColumn HeaderText="SHOP ID" DataField="shopid"></asp:BoundColumn>
                    <asp:BoundColumn HeaderText="SHOP NAME" DataField="name"></asp:BoundColumn>
                    <asp:BoundColumn HeaderText="SHOP TYPE" DataField="shoptype"></asp:BoundColumn>
                    <asp:BoundColumn HeaderText="EMAIL ID" DataField="emailaddress"></asp:BoundColumn>
                    <asp:ButtonColumn Text="APPROVE" CommandName="approve"></asp:ButtonColumn>
                </Columns>
                <FooterStyle BackColor="#C6C3C6" ForeColor="Black" />
                <HeaderStyle BackColor="#4A3C8C" Font-Bold="True" ForeColor="#E7E7FF" />
                <ItemStyle BackColor="#DEDFDE" ForeColor="Black" />
                <PagerStyle BackColor="#C6C3C6" ForeColor="Black" HorizontalAlign="Right" />
                <SelectedItemStyle BackColor="#9471DE" Font-Bold="True" ForeColor="White" />
            </asp:DataGrid>
        </asp:View>
        <br />
        <br />
        <br />
        <asp:View ID="View2" runat="server">
            <table class="style1" bgcolor="Black">
                <tr>
                    <td style="background-color: #FFFFFF; color: #000000;">
                        SHOP ID</td>
                    <td>
                        <asp:TextBox ID="txt_shopid" runat="server" Enabled="False"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td style="background-color: #FFFFFF; color: #000000;">
                        SHOP NAME</td>
                    <td>
                        <asp:TextBox ID="txt_shopname" runat="server" Enabled="False"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="style2" style="background-color: #FFFFFF; color: #000000;">
                        SHOP TYPE</td>
                    <td class="style2">
                        <asp:TextBox ID="txt_shoptype" runat="server" Enabled="False"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td style="background-color: #FFFFFF; color: #000000;">
                        BUILDING NO/NAME</td>
                    <td>
                        <asp:TextBox ID="txt_building" runat="server" Enabled="False"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td style="background-color: #FFFFFF; color: #000000;">
                        PLACE</td>
                    <td>
                        <asp:TextBox ID="txt_place" runat="server" Enabled="False"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td style="background-color: #FFFFFF; color: #000000;">
                        FLOOR</td>
                    <td>
                        <asp:TextBox ID="txt_floor" runat="server" Enabled="False"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td style="background-color: #FFFFFF; color: #000000;">
                        CITY</td>
                    <td>
                        <asp:TextBox ID="txt_city" runat="server" Enabled="False"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td style="background-color: #FFFFFF; color: #000000;">
                        STATE</td>
                    <td>
                        <asp:TextBox ID="txt_state" runat="server" Enabled="False"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td style="background-color: #FFFFFF; color: #000000;">
                        PHONE NUMBER</td>
                    <td>
                        <asp:TextBox ID="txt_phonenumber" runat="server" Enabled="False"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td style="background-color: #FFFFFF; color: #000000;">
                        WEBSITE</td>
                    <td>
                        <asp:TextBox ID="txt_website" runat="server" Enabled="False"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td style="background-color: #FFFFFF; color: #000000;">
                        EMAIL ID</td>
                    <td>
                        <asp:TextBox ID="txt_emailid" runat="server" Enabled="False"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td style="background-color: #FFFFFF; color: #000000;">
                        &nbsp;</td>
                    <td style="color: #FFFFFF; background-color: #FFFFFF;">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td style="background-color: #000000; color: #FFFFFF;">
                        <asp:Button ID="btn_approve" runat="server" Text="APPROVE" 
                            onclick="btn_approve_Click" />
                    </td>
                    <td>
                        <asp:Button ID="btn_reject" runat="server" Text="REJECT" 
                            onclick="btn_reject_Click" />
                    </td>
                </tr>
            </table>
        </asp:View>
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
    </asp:MultiView>
</asp:Content>

