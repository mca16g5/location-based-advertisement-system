﻿<%@ Page Title="" Language="C#" MasterPageFile="~/shop.master" AutoEventWireup="true" CodeFile="shop_news_updation.aspx.cs" Inherits="shop_news_updation" %>

<%--<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>--%>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:MultiView ID="MultiView1" runat="server">
        <asp:View ID="View1" runat="server">
            <table class="style1">
                <tr>
                    <td style="color: #000000; background-color: #FFFFFF">
                        NEWS</td>
                    <td>
                        <asp:TextBox ID="txt_news" runat="server" TextMode="MultiLine"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                            ControlToValidate="txt_news" ErrorMessage="***" ForeColor="#CC0000"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;</td>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr>
                    <td>
                        &nbsp;</td>
                    <td>
                        <asp:Button ID="btn_save" runat="server" onclick="btn_save_Click" Text="SAVE" />
                    </td>
                </tr>
            </table>
        </asp:View>
        <br />
        <br />
        <br />
        <br />
        <asp:View ID="View2" runat="server">
            <table class="style1">
                <tr>
                    <td>
                        <table class="style1">
                            <tr>
                                <td>
                                    <table class="style1">
                                        <tr>
                                            <td style="color: #000000; background-color: #FFFFFF">
                                                SEARCH</td>
                                            <td>
                                                <asp:TextBox ID="txt_search" runat="server" 
                                                    ontextchanged="txt_search_TextChanged"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                                                    ControlToValidate="txt_search" ErrorMessage="***" ForeColor="#CC0000"></asp:RequiredFieldValidator>
                                            </td>
                                            <td>
                                                <asp:Button ID="btn_go" runat="server" Text="GO" onclick="btn_go_Click" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    &nbsp;</td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <asp:DataGrid ID="DataGrid1" runat="server" AutoGenerateColumns="False" 
                onitemcommand="DataGrid1_ItemCommand" BackColor="White" 
                BorderColor="White" BorderStyle="Ridge" BorderWidth="2px" CellPadding="3" 
                CellSpacing="1" GridLines="None" Height="345px" Width="713px">
                <Columns>
                    <asp:BoundColumn HeaderText="NEWS ID" DataField="newsid"></asp:BoundColumn>
                    <asp:BoundColumn HeaderText="SHOP ID" DataField="shopid"></asp:BoundColumn>
                    <asp:BoundColumn HeaderText="NEWS" DataField="newscontent"></asp:BoundColumn>
                    <asp:BoundColumn HeaderText="DATE" DataField="newsdate"></asp:BoundColumn>
                    <asp:ButtonColumn CommandName="EDIT" Text="EDIT"></asp:ButtonColumn>
                    <asp:ButtonColumn CommandName="DELETE" Text="DELETE"></asp:ButtonColumn>
                </Columns>
                <FooterStyle BackColor="#C6C3C6" ForeColor="Black" />
                <HeaderStyle BackColor="#4A3C8C" Font-Bold="True" ForeColor="#E7E7FF" />
                <ItemStyle BackColor="#DEDFDE" ForeColor="Black" />
                <PagerStyle BackColor="#C6C3C6" ForeColor="Black" HorizontalAlign="Right" />
                <SelectedItemStyle BackColor="#9471DE" Font-Bold="True" ForeColor="White" />
            </asp:DataGrid>
            <asp:Button ID="Button1" runat="server" onclick="Button1_Click" 
                Text="add new" CausesValidation="False" />
            <br />
        </asp:View>
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
    </asp:MultiView>
</asp:Content>

