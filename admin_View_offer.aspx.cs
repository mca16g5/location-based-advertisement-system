﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
public partial class admin_View_offer : System.Web.UI.Page
{
    dbop db=new dbop();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            MultiView1.SetActiveView(View1);

            string n = "select name,shopid from shopreg";
           
            DataTable dt = db.ret(n);

            Session["id"] =dt.Rows[0][1].ToString();

            ddl_shop.DataSource = dt;
            ddl_shop.DataTextField = "name";
            ddl_shop.DataValueField = "shopid";
            ddl_shop.DataBind();
            ddl_shop.Items.Insert(0, "select");
        }


    }
    protected void MultiView1_ActiveViewChanged(object sender, EventArgs e)
    {
        
    }
    protected void ddl_shop_SelectedIndexChanged(object sender, EventArgs e)
    {
        string seloffer = "select * from offer where shopid='" +ddl_shop.SelectedValue+ "'";

        DataTable dt1 = db.ret(seloffer);
        DataGrid1.DataSource = dt1;
        DataGrid1. DataBind();


        MultiView1.SetActiveView(View2);

    }
    protected void DataGrid1_ItemCommand(object source, DataGridCommandEventArgs e)
    {
        txt_subject.Text = e.Item.Cells[2].Text;
        MultiView1.SetActiveView(View3);
    }
}