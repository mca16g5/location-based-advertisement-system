﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Publik.master" AutoEventWireup="true" CodeFile="publik_Registration.aspx.cs" Inherits="publik_Registration" %>

<%--<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        .style2
        {
            height: 26px;
        }
        .style3
        {
            height: 29px;
        }
    .style4
    {
        height: 30px;
    }
    </style>
</asp:Content>--%>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    
    <br />
    <table class="style1">
        <tr>
            <td style="color: #000000; background-color: #FFFFFF">
                <asp:Label ID="Label1" runat="server" Text="SHOP NAME"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txt_name" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                    ControlToValidate="txt_name" ErrorMessage="***" ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td style="color: #000000; background-color: #FFFFFF">
                <asp:Label ID="Label2" runat="server" Text="SHOP TYPE"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txt_type" runat="server" Height="21px" Width="129px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                    ControlToValidate="txt_type" ErrorMessage="***" ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td style="color: #000000; background-color: #FFFFFF">
                <asp:Label ID="Label3" runat="server" Text="BUILDING NO/NAME"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txt_building" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" 
                    ControlToValidate="txt_building" ErrorMessage="***" ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td style="color: #000000; background-color: #FFFFFF">
                FLOOR</td>
            <td>
                <asp:TextBox ID="txt_floor" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" 
                    ControlToValidate="txt_floor" ErrorMessage="***" ForeColor="#CC0000"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td style="color: #000000; background-color: #FFFFFF">
                WEB</td>
            <td>
                <asp:TextBox ID="txt_web" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" 
                    ControlToValidate="txt_web" ErrorMessage="***" ForeColor="#CC0000"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="style2" style="color: #000000; background-color: #FFFFFF">
                <asp:Label ID="Label4" runat="server" Text="PLACE"></asp:Label>
            </td>
            <td class="style2">
                <asp:TextBox ID="txt_place" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" 
                    ControlToValidate="txt_place" ErrorMessage="***" ForeColor="#CC0000"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="style3" style="color: #000000; background-color: #FFFFFF">
                <asp:Label ID="Label5" runat="server" Text="CITY"></asp:Label>
            </td>
            <td class="style3">
                <asp:TextBox ID="txt_city" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" 
                    ControlToValidate="txt_city" ErrorMessage="***" ForeColor="#CC0000"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td style="color: #000000; background-color: #FFFFFF">
                <asp:Label ID="Label6" runat="server" Text="STATE"></asp:Label>
            </td>
            <td>
                <asp:DropDownList ID="ddl_state" runat="server" 
                    onselectedindexchanged="ddl_state_SelectedIndexChanged">
                    <asp:ListItem>select</asp:ListItem>
                    <asp:ListItem>kerala</asp:ListItem>
                    <asp:ListItem>tamil nadu</asp:ListItem>
                    <asp:ListItem>karnadaka</asp:ListItem>
                    <asp:ListItem>andra predesh</asp:ListItem>
                    <asp:ListItem>gujarath</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td style="color: #000000; background-color: #FFFFFF">
                <asp:Label ID="Label7" runat="server" Text="PHONE NUMBER"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txt_phonenumber" runat="server" 
                    ontextchanged="txt_phonenumber_TextChanged"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" 
                    ControlToValidate="txt_phonenumber" ErrorMessage="***" ForeColor="#CC0000"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" 
                    ControlToValidate="txt_phonenumber" ErrorMessage="invalid phone number" 
                    ValidationExpression="\d{10}"></asp:RegularExpressionValidator>
            </td>
        </tr>
        <tr>
            <td style="color: #000000; background-color: #FFFFFF">
                <asp:Label ID="Label8" runat="server" Text="EMAIL ADDRESS"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txt_emailaddress" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" 
                    ControlToValidate="txt_emailaddress" ErrorMessage="***" ForeColor="#CC0000"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" 
                    ControlToValidate="txt_emailaddress" ErrorMessage="invalid emailid" 
                    ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
            </td>
        </tr>
        <tr>
            <td class="style2" style="color: #000000; background-color: #FFFFFF">
                <asp:Label ID="Label9" runat="server" Text="PASSWORD"></asp:Label>
            </td>
            <td class="style2">
                <asp:TextBox ID="txt_pssword" runat="server" TextMode="Password"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" 
                    ControlToValidate="txt_pssword" ErrorMessage="***" ForeColor="#CC0000"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="style4" style="color: #000000; background-color: #FFFFFF">
                <asp:Label ID="Label10" runat="server" Text="RE ENTER PASSWORD"></asp:Label>
            </td>
            <td class="style4">
                <asp:TextBox ID="txt_reenterpassword" runat="server" TextMode="Password"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator13" runat="server" 
                    ControlToValidate="txt_reenterpassword" ErrorMessage="***" ForeColor="#CC0000"></asp:RequiredFieldValidator>
                <asp:CompareValidator ID="CompareValidator1" runat="server" 
                    ControlToCompare="txt_pssword" ControlToValidate="txt_reenterpassword" 
                    ErrorMessage="password mismatch"></asp:CompareValidator>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                <asp:Button ID="btn_signup" runat="server" Text="SIGN UP" 
                    onclick="btn_signup_Click" />
            </td>
        </tr>
    </table>
</asp:Content>

