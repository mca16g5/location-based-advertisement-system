﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class admin_Shop_approval : System.Web.UI.Page
{
    dbop db = new dbop();
    string s;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            s = "select * from shopreg where status='pending'";
            DataTable dt = db.ret(s);
            DataGrid1.DataSource = dt;
            DataGrid1.DataBind();
            MultiView1.SetActiveView(View1);
        }
    }
    protected void DataGrid1_ItemCommand(object source, DataGridCommandEventArgs e)
    {
        if (e.CommandName == "approve")
        {
            s = "select * from shopreg where shopid="+Convert.ToInt32(e.Item.Cells[0].Text)+"";
            DataTable dt = db.ret(s);
            txt_shopid.Text = dt.Rows[0][0].ToString();
            txt_shopname.Text = dt.Rows[0][1].ToString();
            txt_shoptype.Text = dt.Rows[0][2].ToString();
            txt_building.Text = dt.Rows[0][3].ToString();
            txt_floor.Text = dt.Rows[0][4].ToString();
            txt_website.Text = dt.Rows[0][9].ToString();
            txt_emailid.Text = dt.Rows[0][10].ToString();
            txt_place.Text = dt.Rows[0][5].ToString();
            txt_city.Text = dt.Rows[0][6].ToString();
            txt_state.Text = dt.Rows[0][7].ToString();
            txt_phonenumber.Text = dt.Rows[0][8].ToString();

            MultiView1.SetActiveView(View2);
        }
    }
    protected void MultiView1_ActiveViewChanged(object sender, EventArgs e)
    {

    }
    protected void btn_approve_Click(object sender, EventArgs e)
    {
        string s = "update shopreg set status ='approved' where shopid="+txt_shopid.Text+"";

            db.nonret(s);
            Response.Redirect("admin_Shop_approval.aspx");

    }
    protected void btn_reject_Click(object sender, EventArgs e)
    {
        string s ="update shopreg set status='reject' where shopid="+txt_shopid.Text+"";
        db.nonret(s);
        Response.Redirect("admin_Shop_approval.aspx");
    }
}