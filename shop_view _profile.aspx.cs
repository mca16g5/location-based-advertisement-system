﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
public partial class shop_view__profile : System.Web.UI.Page
{
    dbop db = new dbop();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            string d = "SELECT * from shopreg where shopid='" + Session["sid"].ToString() + "'";
            DataTable dt = db.ret(d);
            Session["dt"] = dt;
            lbl_name.Text = dt.Rows[0][1].ToString();
            lbl_shoptype.Text = dt.Rows[0][2].ToString();
            lbl_building.Text = dt.Rows[0][3].ToString();
            txt_place.Text = dt.Rows[0][5].ToString();
            lbl_phonenumber.Text = dt.Rows[0][8].ToString();
            lbl_website.Text = dt.Rows[0][9].ToString();
            lbl_emailid.Text = dt.Rows[0][10].ToString();
        }
    }

    
    protected void btn_edit_Click(object sender, EventArgs e)
    {
        Response.Redirect("shop_profile_change.aspx");
    }}