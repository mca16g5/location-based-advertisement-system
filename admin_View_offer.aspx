﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin.master" AutoEventWireup="true" CodeFile="admin_View_offer.aspx.cs" Inherits="admin_View_offer" %>

<%--<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>--%>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:MultiView ID="MultiView1" runat="server" 
        onactiveviewchanged="MultiView1_ActiveViewChanged">
        <br />
        <asp:View ID="View1" runat="server">
            <table class="style1">
                <tr>
                    <td>
                        Select shop name</td>
                    <td>
                        &nbsp;</td>
                    <td>
                        &nbsp;</td>
                    <td>
                        &nbsp;</td>
                    <td style="color: #000000; background-color: #FFFFFF">
                        &nbsp;</td>
                    <td>
                        <asp:DropDownList ID="ddl_shop" runat="server" AutoPostBack="True" 
                            Height="59px" onselectedindexchanged="ddl_shop_SelectedIndexChanged" 
                            Width="384px">
                        </asp:DropDownList>
                    </td>
                    <td>
                        &nbsp;</td>
                </tr>
            </table>
        </asp:View>
        <br />
        <br />
        <asp:View ID="View2" runat="server">
            <br />
            <asp:DataGrid ID="DataGrid1" runat="server" AutoGenerateColumns="False" 
                onitemcommand="DataGrid1_ItemCommand"     BackColor="White" BorderColor="White" BorderStyle="Ridge" BorderWidth="2px" 
                            GridLines="None" style="margin-top: 0px" Width="651px">
                <Columns>
                    <asp:BoundColumn DataField="offerid" HeaderText="OFFER ID"></asp:BoundColumn>
                    <asp:BoundColumn DataField="shopid" HeaderText="SHOPID"></asp:BoundColumn>
                    <asp:BoundColumn DataField="subject" HeaderText="SUB"></asp:BoundColumn>
                    <asp:BoundColumn DataField="offers" HeaderText="OFFER"></asp:BoundColumn>
                    <asp:BoundColumn DataField="validfrom" HeaderText="VALIDFROM"></asp:BoundColumn>
                    <asp:BoundColumn DataField="validto" HeaderText="VALID2"></asp:BoundColumn>
                    <asp:BoundColumn DataField="date" HeaderText="DATE"></asp:BoundColumn>
                </Columns>
                <FooterStyle BackColor="#C6C3C6" ForeColor="Black" />
                <HeaderStyle BackColor="#4A3C8C" Font-Bold="True" ForeColor="#E7E7FF" />
                <ItemStyle BackColor="#DEDFDE" ForeColor="Black" />
                <PagerStyle BackColor="#C6C3C6" ForeColor="Black" HorizontalAlign="Right" />
                <SelectedItemStyle BackColor="#9471DE" Font-Bold="True" ForeColor="White" />
            </asp:DataGrid>
            <br />
            <br />
        </asp:View>
        <br />
        <asp:View ID="View3" runat="server">
            <table class="style1">
                <tr>
                    <td style="color: #000000; background-color: #FFFFFF">
                        SUBJECT</td>
                    <td colspan="3">
                        <asp:TextBox ID="txt_subject" runat="server" Enabled="False"></asp:TextBox>
                    </td>
                    <td style="color: #000000; background-color: #FFFFFF">
                        DATE</td>
                    <td>
                        &nbsp;</td>
                    <td>
                        &nbsp;</td>
                    <td>
                        <asp:TextBox ID="txt_date" runat="server" Enabled="False"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td style="color: #000000; background-color: #FFFFFF">
                        OFFER</td>
                    <td colspan="3">
                        <asp:TextBox ID="txt_subject0" runat="server" Enabled="False"></asp:TextBox>
                    </td>
                    <td>
                        &nbsp;</td>
                    <td>
                        &nbsp;</td>
                    <td>
                        &nbsp;</td>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr>
                    <td style="color: #000000; background-color: #FFFFFF">
                        VALID FROM</td>
                    <td>
                        <asp:TextBox ID="txt_validfrom" runat="server" Enabled="False"></asp:TextBox>
                    </td>
                    <td>
                        &nbsp;</td>
                    <td>
                    </td>
                    <td>
                        &nbsp;</td>
                    <td colspan="3">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td style="color: #000000; background-color: #FFFFFF">
                        VALID TO</td>
                    <td>
                        <asp:TextBox ID="txt_validto" runat="server" Enabled="False"></asp:TextBox>
                    </td>
                    <td>
                        &nbsp;</td>
                    <td>
                        &nbsp;</td>
                    <td>
                        &nbsp;</td>
                    <td colspan="3">
                        &nbsp;</td>
                </tr>
            </table>
            <br />
            <br />
        </asp:View>
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
    </asp:MultiView>
</asp:Content>

