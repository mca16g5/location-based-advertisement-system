﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin.master" AutoEventWireup="true" CodeFile="admin_View_feedback.aspx.cs" Inherits="admin_View_feedback" %>

<%--<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>--%>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:DataGrid ID="DataGrid1" runat="server" AutoGenerateColumns="False" style="margin-left: 12px; margin-right: 127px;" 
        Width="819px" onitemcommand="DataGrid1_ItemCommand" BackColor="White" 
        BorderColor="#999999" BorderStyle="None" BorderWidth="1px" 
        GridLines="Vertical">
        <AlternatingItemStyle BackColor="#DCDCDC" />
        <Columns>
            <asp:BoundColumn HeaderText="FEEDBACK ID" DataField="feedbackid"></asp:BoundColumn>
            <asp:BoundColumn HeaderText="FEEDBACK" DataField="feedback"></asp:BoundColumn>
            <asp:BoundColumn HeaderText="EMAIL ID" DataField="emailaddress"></asp:BoundColumn>
            <asp:BoundColumn HeaderText="DATE" DataField="feedbackdate"></asp:BoundColumn>
            <asp:ButtonColumn Text="Delete"></asp:ButtonColumn>
        </Columns>
        <FooterStyle BackColor="#CCCCCC" ForeColor="Black" />
        <HeaderStyle BackColor="#000084" Font-Bold="True" ForeColor="White" />
        <ItemStyle BackColor="#EEEEEE" ForeColor="Black" />
        <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" 
            Mode="NumericPages" />
        <SelectedItemStyle BackColor="#008A8C" Font-Bold="True" ForeColor="White" />
    </asp:DataGrid>
</asp:Content>

