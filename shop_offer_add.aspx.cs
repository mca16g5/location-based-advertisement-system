﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class shop_offer_add : System.Web.UI.Page
{
    dbop db = new dbop();
    
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            for (int i = 1; i < 13; i++)
            {

                ddl_datemonth.Items.Add(i.ToString());
            }

            for (int j = 1; j < 32; j++)
            {
                ddl_dateday.Items.Add(j.ToString());
            }
            for (int k = 2000; k < 2030; k++)
            {
                ddl_dateyear.Items.Add(k.ToString());
            }
            for (int m = 1; m < 13; m++)
            {
                ddl_dm.Items.Add(m.ToString());
            }
            for (int n = 1; n < 32; n++)
            {
                ddl_dd.Items.Add(n.ToString());
            }
            for (int s = 2000; s < 2030; s++)
            {
                ddl_dy.Items.Add(s.ToString());
            }
            ddl_dateday.Items.Insert(0,"Day");
            ddl_datemonth.Items.Insert(0, "Month");
            ddl_dateyear.Items.Insert(0, "Year");
            ddl_dd.Items.Insert(0, "Day");
            ddl_dm.Items.Insert(0, "Month");
            ddl_dy.Items.Insert(0, "Year");
           
        }
    }
    protected void btn_save_Click(object sender, EventArgs e)
    {
        
        
       string s = "select max(offerid) from offer";

        int ofr_id = db.max_id(s);
        string from_date =ddl_datemonth.Text+"/"+ddl_dateday.Text+"/"+ddl_dateyear.Text;
            string to_date=ddl_dm.Text+"/"+ddl_dd.Text+"/"+ddl_dy.Text;
            string dd = System.DateTime.Now.ToShortDateString();
            string d1 = from_date.ToString(); ;
            string  d2=to_date.ToString();
        s = "Insert into offer values(" + ofr_id + "," + Session["sid"].ToString() + ",'" + txt_subject.Text + "','" + txt_offer.Text + "','" + d1 + "','" + d2 + "','" +dd+ "')";
        db.nonret(s);
        Response.Write("<script>alert('inserted...')</script>");
        ddl_dd.SelectedIndex = 0;
        ddl_dateday.SelectedIndex = 0;
        ddl_datemonth.SelectedIndex = 0;
        ddl_dateyear.SelectedIndex = 0;
        ddl_dm.SelectedIndex = 0;
        ddl_dy.SelectedIndex = 0;
        txt_offer.Text = "";
        txt_subject.Text = "";



    }
    protected void ddl_dateday_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
}