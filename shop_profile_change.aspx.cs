﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class shop_profile_change : System.Web.UI.Page
{
    dbop db=new dbop();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            DataTable dt = (DataTable)Session["dt"];
            txt_name.Text = dt.Rows[0][1].ToString();
            txt_shoptype.Text = dt.Rows[0][2].ToString();
            txt_building.Text = dt.Rows[0][3].ToString();
            txt_place.Text = dt.Rows[0][5].ToString();
            txt_phonenumber.Text = dt.Rows[0][8].ToString();
            txt_website.Text = dt.Rows[0][9].ToString();
            txt_emailid.Text = dt.Rows[0][10].ToString();
        }

    }
    protected void btn_save_Click(object sender, EventArgs e)
    {
        
        string f ="update shopreg set name='"+txt_name.Text+"',shoptype='"+txt_shoptype.Text+"',building='"+txt_building.Text+"',place='"+txt_place.Text+"',phonenumber='"+txt_phonenumber.Text+"',websiteaddress='"+txt_website.Text+"',emailaddress='"+txt_emailid.Text+"' where shopid="+Session["sid"].ToString()+"";
        db.nonret(f);
        Response.Redirect("shop_view _profile.aspx");
    }
}