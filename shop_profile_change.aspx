﻿<%@ Page Title="" Language="C#" MasterPageFile="~/shop.master" AutoEventWireup="true" CodeFile="shop_profile_change.aspx.cs" Inherits="shop_profile_change" %>

<%--<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>--%>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <table class="style1">
        <tr>
            <td style="color: #000000; background-color: #FFFFFF">
                NAME</td>
            <td>
                <asp:TextBox ID="txt_name" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td style="color: #000000; background-color: #FFFFFF">
                SHOPTYPE</td>
            <td>
                <asp:TextBox ID="txt_shoptype" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td style="color: #000000; background-color: #FFFFFF">
                BUILDING NUMBER/NAME</td>
            <td>
                <asp:TextBox ID="txt_building" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td style="color: #000000; background-color: #FFFFFF">
                PLACE</td>
            <td>
                <asp:TextBox ID="txt_place" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td style="color: #000000; background-color: #FFFFFF">
                PHONE NUMBER</td>
            <td>
                <asp:TextBox ID="txt_phonenumber" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td style="color: #000000; background-color: #FFFFFF">
                WEBSITE</td>
            <td>
                <asp:TextBox ID="txt_website" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td style="color: #000000; background-color: #FFFFFF">
                EMAIL ID</td>
            <td>
                <asp:TextBox ID="txt_emailid" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                <asp:Button ID="btn_save" runat="server" Text="SAVE" onclick="btn_save_Click" />
            </td>
        </tr>
    </table>
</asp:Content>

