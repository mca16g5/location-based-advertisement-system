﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
public partial class shop_view_offer : System.Web.UI.Page
{
    dbop db =new dbop();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            MultiView1.SetActiveView(View1);
            string s = "SELECT * from offer";
                DataTable dt=db.ret(s);
            DataGrid1.DataSource=dt;
            DataGrid1.DataBind();
            for (int i = 1; i < 32; i++)
            {
                DropDownList1.Items.Add(i.ToString());
            }
            for (int i = 1; i <13; i++)
            {
                DropDownList2.Items.Add(i.ToString());
            }
            for (int i = 2000; i <2030; i++)
            {
                DropDownList3.Items.Add(i.ToString());
            }
            for (int i = 1; i < 32; i++)
            {
                DropDownList4.Items.Add(i.ToString());
            }
            for (int i = 1; i < 13; i++)
            {
                DropDownList5.Items.Add(i.ToString());
            }
            for (int i = 2000; i < 2030; i++)
            {
                DropDownList6.Items.Add(i.ToString());
            }
            DropDownList1.Items.Insert(0, "Day");
            DropDownList2.Items.Insert(0, "Month");
            DropDownList3.Items.Insert(0, "Year");
            DropDownList4.Items.Insert(0, "Day");
            DropDownList5.Items.Insert(0, "Month");
            DropDownList6.Items.Insert(0,"Year");
        }
    }
    protected void DataGrid1_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void DataGrid1_ItemCommand(object source, DataGridCommandEventArgs e)
    {
      
        if (e.CommandName == "EDIT")
        {
            Session["o_id"] = e.Item.Cells[0].Text;
            TextBox2.Text = e.Item.Cells[1].Text;
            string[] d1 = e.Item.Cells[2].Text.Split('/',' ');
            DropDownList2.SelectedValue = d1[0].ToString();
            DropDownList1.SelectedValue = d1[1].ToString();
            DropDownList3.SelectedValue = d1[2].ToString();
            string[] d2 = e.Item.Cells[3].Text.Split('/', ' ');
            DropDownList5.SelectedValue = d2[0].ToString();
            DropDownList4.SelectedValue = d2[1].ToString();
            DropDownList6.SelectedValue = d2[2].ToString();
           // TextBox1.Text = e.Item.Cells[1].Text;
            //TextBox2.Text = e.Item.Cells[2].Text;
            //TextBox3.Text = e.Item.Cells[3].Text;
            MultiView1.SetActiveView(View2);
        }
        else
        {
            string s = "delete from offer where offerid=" + e.Item.Cells[0].Text+"";
            db.nonret(s);
            string n = "SELECT * from offer";
            DataTable dt = db.ret(n);
            DataGrid1.DataSource = dt;
            DataGrid1.DataBind();
            MultiView1.SetActiveView(View1);

        }
     
    }
protected void  btn_update_Click(object sender, EventArgs e)
{
    string d1 = (DropDownList2.Text + "/" + DropDownList1.Text + "/" + DropDownList3.Text);
    string d2 = (DropDownList5.Text + "/" + DropDownList4.Text + "/" + DropDownList6.Text);

    string s = "update offer set offers='" + TextBox2.Text + "',validfrom='" + d1 + "',validto='" + d2 + "' where offerid=" + Session["o_id"].ToString()+ " ";
    db.nonret(s);
    Response.Write("<script>alert('updated...')</script>");
    Response.Redirect("shop_view_offer.aspx");
}
}