﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class shop_news_updation : System.Web.UI.Page
{
    public void gridview()
{
     MultiView1.SetActiveView(View2);
        string s = "select * from news";
        DataTable dt = db.ret(s);
        DataGrid1.DataSource = dt;
        DataGrid1.DataBind();
}

    protected void Page_Load(object sender, EventArgs e)
    {


        if (!IsPostBack)
        {
            gridview();
        }
   
    }
    dbop db = new dbop();
    string sid;
    protected void btn_save_Click(object sender, EventArgs e)
    {
        string uname = Session["uname"].ToString();
        string t="select shopid from shopreg where emailaddress='"+uname+"'";
        DataTable dt = db.ret(t);
        if (dt.Rows.Count > 0)
        {
            DataRow dr = dt.Rows[0];
         sid = dr[0].ToString();
        }
        if (btn_save.Text == "SAVE")
        {
            // string date = ddl_dateday.Text + "/" + ddl_datemonth.Text + "/" + ddl_dateyear.Text;
            string s = "select max(newsid) from news";
            string nid = db.max_id(s).ToString();
            string date = System.DateTime.Now.ToShortDateString();
            string qry = "insert into news values(" + nid + "," + sid + ",'" + txt_news.Text + "','" + date + "')";
            db.nonret(qry);
            Response.Write("<script>alert('Inserted successfully')</script>");
        }
        else
        {
            string d = "update news set newscontent='" + txt_news.Text + "'where newsid=" + Session["id"] + "";
            db.nonret(d);
            Response.Write("<script>alert('Inserted successfully')</script>");
        }
        gridview();
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        MultiView1.SetActiveView(View1);
        txt_news.Text = "";
    }
    protected void DataGrid1_ItemCommand(object source, DataGridCommandEventArgs e)
    {
        if (e.CommandName == "EDIT")
        {
            MultiView1.SetActiveView(View1);
            txt_news.Text = e.Item.Cells[2].Text;
            btn_save.Text = "update";
            Session["id"] = e.Item.Cells[0].Text;
        }
        else
        {
            string r = "delete from news where newsid='"+Convert.ToInt16(e.Item.Cells[0].Text)+"'";
            db.nonret(r);
            gridview();
        }

    }
    protected void txt_search_TextChanged(object sender, EventArgs e)
    {
       // string s = "SELECT newsid, shopid, newscontent, newsdate FROM     news WHERE  (newsid LIKE '% '"+txt_search+"' %')";
    }
    protected void btn_go_Click(object sender, EventArgs e)
    {

        string s = "SELECT newsid, shopid, newscontent, newsdate FROM     news WHERE ( (newsid LIKE '%" + txt_search.Text + "%') or (shopid LIKE '% " + txt_search.Text + " %') or (newscontent LIKE '% " + txt_search.Text + " %') or (newsdate LIKE '% " + txt_search.Text + " %') ) ";
        DataTable dt = db.ret(s);
        DataGrid1.DataSource = dt;
        DataGrid1.DataBind();
    }
}