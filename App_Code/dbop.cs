﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;

/// <summary>
/// Summary description for dbop
/// </summary>
public class dbop
{
	public dbop()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    SqlConnection con = new SqlConnection(@"Data Source=.\SQLEXPRESS;AttachDbFilename=C:\inetpub\wwwroot\location based add\App_Data\loc.mdf;Integrated Security=True;Connect Timeout=30;User Instance=True");
    public void nonret(string s)
    {
        SqlCommand cmd = new SqlCommand();
        cmd.Connection = con;
        cmd.CommandText = s;
        con.Open();
        cmd.ExecuteNonQuery();
        con.Close();


    }
    public DataTable ret(string s)
    {
        SqlCommand cmd = new SqlCommand();
        cmd.Connection = con;
        cmd.CommandText = s;
        SqlDataAdapter da = new SqlDataAdapter();
        da.SelectCommand = cmd;
        DataTable dt = new DataTable();
        da.Fill(dt);
        return dt;
    }
    public int max_id(string s)
    {
        SqlCommand cmd=new SqlCommand();
        cmd.Connection = con;
        cmd.CommandText = s;
        int mxid;
        con.Open();
        try
        {
            mxid = int.Parse(cmd.ExecuteScalar().ToString()) + 1;
        }
        catch
        {
            mxid = 1;
        }
        finally
        {
            con.Close();
        }
        return mxid;
    }
}