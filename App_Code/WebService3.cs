﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Data;
using System.Data.SqlClient;


/// <summary>
/// Summary description for WebService3
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]
public class WebService3 : System.Web.Services.WebService
{

    public WebService3()
    {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }
    dbop db = new dbop();
    [WebMethod]
    public string HelloWorld()
    {
        return "Hello World";
    }
    [WebMethod]
    public string insert(string userid, string feedbackdate, string feedback)
    {
        try
        {
            string c = "select max(feedbackid)from feedback";
            string max = db.max_id(c).ToString();
            string a = "insert into feedback values(" + max + ",'" + userid + "','" + feedbackdate + "','" + feedback + "')";
            db.nonret(a);
            return "ok";

        }
        catch
        {
            return "not ok";

        }


    }
    [WebMethod]
    public string selshop(string place)
    {
        string detail = "";
        string qr = "select shopid,name from shopreg where place='" + place + "'";
        DataTable dt = db.ret(qr);
        if (dt.Rows.Count > 0)
        {
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                detail = detail + dt.Rows[i][0].ToString() + "#" + dt.Rows[i][1].ToString() + "@";
            }
            return detail;
        }
        else
        {
            return "no value";
        }



    }





    [WebMethod]
    public string selectnews(string shopid)
    {
        string detail = "";
        string a = "select * from news where shopid="+shopid+"";
        DataTable dt = db.ret(a);
        if (dt.Rows.Count > 0)
        {

            try
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {

                    detail = detail + dt.Rows[i][0].ToString() + "#" + dt.Rows[i][1].ToString() + "#" + dt.Rows[0][2].ToString() + "#" + dt.Rows[0][3].ToString() + "@";
                }
                string result;
                result = dt.Rows[0][0].ToString() + "#" + dt.Rows[0][1].ToString() + "#" + dt.Rows[0][2].ToString() +  "#" + dt.Rows[0][3].ToString()+ "@";
                return result;
            }
            catch
            {
                return "no result";
            }
        }
        else
        {
            return "no result";
        }
    }
    [WebMethod]

    public string selectoffer(string shopid)
    {
        string result = "";
        string a = "select * from offer where shopid=" + shopid + "";
        DataTable dt = db.ret(a);
        if (dt.Rows.Count > 0)
        {

            try
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {

                    string re = dt.Rows[i][0].ToString() + "#" + dt.Rows[i][1].ToString() + "#" + dt.Rows[i][2].ToString() + "#" + dt.Rows[i][3].ToString() + "#" + dt.Rows[i][4].ToString() + "#" + dt.Rows[i][5].ToString() + "#" + dt.Rows[i][6].ToString() + "@";
                    result =result + re;
                }
            }
            catch
            {
                result = "no details";
            }
        }
        else
        {
            return "no result";
        }
        return result;
    }

    [WebMethod]

    public string selectshopreg(string shopid)
    {
        string a = "select * from shopreg ";
         a = "select * from shopreg where shopid=" + shopid + "";
        DataTable dt = db.ret(a);
        if (dt.Rows.Count > 0)
        {

            try
            {
                string result;
                result = dt.Rows[0][0].ToString() + "#" + dt.Rows[0][1].ToString() + "#" + dt.Rows[0][2].ToString() + "#" + dt.Rows[0][3].ToString() + "#" + dt.Rows[0][4].ToString() + "#" + dt.Rows[0][5].ToString() + "#" + dt.Rows[0][6].ToString() + "#" + dt.Rows[0][7].ToString() + "#" + dt.Rows[0][8].ToString() + "#" + dt.Rows[0][9].ToString() + "#" + dt.Rows[0][10].ToString() + "#" + dt.Rows[0][11].ToString();
                return result;
            }
            catch
            {
                return "no result";
            }
        }
        else
        {
            return "no result";
        }
    }

    [WebMethod]
    public string insuser(string name, string phn, string email, string dob, string uname, string pswd)
    {
        try
        {
            string qry = "select max(userid) from userreg";
            int id = db.max_id(qry);

            qry = "insert into userreg values('" + id + "','" + name + "','" + phn + "','" + email + "','" + dob + "')";
            db.nonret(qry);
            qry = "insert into login values('" + email + "','" + pswd + "','user')";
            db.nonret(qry);
            return "ok";
        }
        catch
        {
            return "not";
        }


    }

    [WebMethod]

    public string selectshopreg1(string shopid, string name)
    {
        string result = "";
        string a = "select shopid,name from shopreg";
        DataTable dt = db.ret(a);
        if (dt.Rows.Count > 0)
        {

            try
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {

                    string re = dt.Rows[i][0].ToString() + "#" + dt.Rows[i][1].ToString() + "@";
                    result = re;
                }
            }
            catch
            {
                result = "no details";
            }
        }
        else
        {
            return "no result";
        }
        return result;
    }
    string s = "";
    [WebMethod]
    public string login(string uname, string pwd)
    {
        string result = "SELECT userreg.userid FROM login INNER JOIN userreg ON login.username = userreg.emailaddress where(login.username='" + uname + "' and login.password='"+pwd+"')";
        DataTable dt = db.ret(result);
        if (dt.Rows.Count > 0)
        {
            s = dt.Rows[0][0].ToString();
        }
        else
        {
            s = "no";
        }
        return s;
    }
}