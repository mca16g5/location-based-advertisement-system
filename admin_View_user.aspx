﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin.master" AutoEventWireup="true" CodeFile="admin_View_user.aspx.cs" Inherits="admin_View_user" %>

<%--<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>--%>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:MultiView ID="MultiView1" runat="server">
        <asp:View ID="View1" runat="server">
            <table class="style1">
                <tr>
                    <td style="color: #000000; background-color: #FFFFFF">
                        SEARCH</td>
                    <td>
                        <asp:TextBox ID="txt_search" runat="server" 
                            ontextchanged="txt_search_TextChanged"></asp:TextBox>
                    </td>
                    <td>
                        <asp:Button ID="btn_go" runat="server" Text="GO" onclick="btn_go_Click" />
                    </td>
                </tr>
                <tr>
                    <td style="color: #000000; background-color: #FFFFFF">
                        &nbsp;</td>
                    <td>
                        <asp:DataGrid ID="DataGrid1" runat="server" AutoGenerateColumns="False" 
                            BackColor="White" BorderColor="White" BorderStyle="Ridge" BorderWidth="2px" 
                            GridLines="None" style="margin-top: 0px" Width="651px">
                            <Columns>
                                <asp:BoundColumn DataField="userid" HeaderText="USER ID"></asp:BoundColumn>
                                <asp:BoundColumn DataField="name" HeaderText="NAME"></asp:BoundColumn>
                                <asp:BoundColumn DataField="phonenumber" HeaderText="PHONE NUMBER">
                                </asp:BoundColumn>
                                <asp:BoundColumn DataField="emailaddress" HeaderText="EMAIL ID">
                                </asp:BoundColumn>
                            </Columns>
                            <FooterStyle BackColor="#C6C3C6" ForeColor="Black" />
                            <HeaderStyle BackColor="#4A3C8C" Font-Bold="True" ForeColor="#E7E7FF" />
                            <ItemStyle BackColor="#DEDFDE" ForeColor="Black" />
                            <PagerStyle BackColor="#C6C3C6" ForeColor="Black" HorizontalAlign="Right" />
                            <SelectedItemStyle BackColor="#9471DE" Font-Bold="True" ForeColor="White" />
                        </asp:DataGrid>
                    </td>
                    <td>
                        &nbsp;</td>
                </tr>
            </table>
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
        </asp:View>
    </asp:MultiView>
</asp:Content>

