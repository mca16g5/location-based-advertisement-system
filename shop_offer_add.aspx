﻿<%@ Page Title="" Language="C#" MasterPageFile="~/shop.master" AutoEventWireup="true" CodeFile="shop_offer_add.aspx.cs" Inherits="shop_offer_add" %>

<%--<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        .style2
        {
            width: 94px;
        }
        .style3
        {
            width: 88px;
        }
        .style4
        {
            width: 147px;
        }
    </style>
</asp:Content>--%>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <br />
    <table class="style1">
        <tr>
            <td class="style4" style="color: #000000; background-color: #FFFFFF">
                SUBJECT</td>
            <td colspan="3">
                <asp:TextBox ID="txt_subject" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                    ControlToValidate="txt_subject" ErrorMessage="***" ForeColor="#CC0000"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="style4" style="color: #000000; background-color: #FFFFFF">
                OFFER</td>
            <td colspan="3">
                <asp:TextBox ID="txt_offer" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                    ControlToValidate="txt_offer" ErrorMessage="***" ForeColor="#CC0000"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="style4" style="color: #000000; background-color: #FFFFFF">
                VALID FROM</td>
            <td class="style2">
                <asp:DropDownList ID="ddl_dateday" runat="server" 
                    onselectedindexchanged="ddl_dateday_SelectedIndexChanged">
                </asp:DropDownList>
            </td>
            <td class="style3">
                <asp:DropDownList ID="ddl_datemonth" runat="server">
                </asp:DropDownList>
            </td>
            <td>
                <asp:DropDownList ID="ddl_dateyear" runat="server">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="style4" style="color: #000000; background-color: #FFFFFF">
                VALID TO</td>
            <td class="style2">
                <asp:DropDownList ID="ddl_dd" runat="server">
                </asp:DropDownList>
            </td>
            <td class="style3">
                <asp:DropDownList ID="ddl_dm" runat="server">
                </asp:DropDownList>
            </td>
            <td>
                <asp:DropDownList ID="ddl_dy" runat="server">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="style4">
                &nbsp;</td>
            <td colspan="3">
                <asp:Button ID="btn_save" runat="server" Text="SAVE" onclick="btn_save_Click" />
            </td>
        </tr>
    </table>
</asp:Content>

